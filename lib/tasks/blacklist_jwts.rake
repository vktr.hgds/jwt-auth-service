namespace :blacklist_jwts do
  desc "Delete expired JWT tokens"
  task delete_expired: :environment do
    BlacklistJwt.where(['expiry_date < ?', DateTime.now]).destroy_all

    p "Expired JWTs are deleted at: #{DateTime.now}"
  end
end
