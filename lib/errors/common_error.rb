module Errors
  class CommonError < StandardError
    attr_reader :status_code
    attr_reader :message

    def initialize(message = nil, status_code)
      super(message)

      @message = message
      @status_code = status_code
    end
  end
end