module Tokens
  module RefreshTokens
    module Validators
      module RefreshTokenValidator
        class << self
          attr_accessor :refresh_token

          # check if the:
          # 1. grant_type parameter exists
          # 2. grant_type contains the refresh_token string
          # 3. grant_type's value contains the valid format for the refresh_token
          # 4. tokens is found in the db
          # 5. tokens is not expired
          def validate_and_get_refresh_token(token)
            raise Errors::CommonError.new({ error: I18n.t('auth.missing_grant_type') }, 401) unless token

            token_split = token.split('&')
            raise Errors::CommonError.new({ error: I18n.t('auth.missing_refresh_token') }, 401) if missing_refresh_token?(token_split)

            refresh_token_split = token_split.last.split('=')
            raise Errors::CommonError.new({ error: I18n.t('auth.invalid_token_format') }, 401) if invalid_token_format?(refresh_token_split)

            refresh_token = RefreshToken.find_by_token(refresh_token_split.last)
            raise Errors::CommonError.new({ error: I18n.t('token.not_found')}, 404) unless refresh_token

            if token_expired?(refresh_token)
              refresh_token.destroy
              raise Errors::CommonError.new({ error: I18n.t('token.refresh_token_expired')}, 410)
            end

            refresh_token
          end

          private

          # check if we have a refresh_token text as first word in the grant_type parameter
          # refresh_token format: grant_type=refresh_token&refresh_token=XYZ
          def missing_refresh_token?(token_split)
            return true unless token_split.first.eql? I18n.t('token.refresh_token')

            false
          end

          # check if the grant_type param's refresh_token part is well-formatted
          # refresh_token format: grant_type=refresh_token&refresh_token=XYZ
          # grant_type_token_parts contains the following format: refresh_token=YXZ
          def invalid_token_format?(refresh_token_split)
            grant_type_token_parts = 2

            return true unless refresh_token_split.size == grant_type_token_parts
            return true unless refresh_token_split.first.eql? I18n.t('token.refresh_token')

            false
          end

          # check if refresh tokens is expired
          def token_expired?(token)
            return true if token.expiry_date < DateTime.now

            false
          end
        end
      end
    end
  end
end