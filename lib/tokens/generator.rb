module Tokens
  module Generator
    class << self
      # Creates refresh tokens and JWT
      def call(user:)
        refresh_token = RefreshToken.new(user_id: user&.id)
        raise Errors::CommonError.new({ error: I18n.t('token.save_failed') }, 500) unless refresh_token.save

        access_token = Tokens::JsonWebToken.encode(user_id: user&.id)
        current_time = DateTime.now
        success_response = Tokens::Jwt::Generators::SuccessResponseGenerator
                             .call(user, access_token, refresh_token, current_time).as_json

        [success_response, 200]
      end
    end
  end
end