module Tokens
  module Jwt
    module Generators
      module SuccessResponseGenerator
        module_function

        def call(user, access_token, refresh_token, current_time)
          Tokens::Jwt::Responses::SuccessResponse.new.tap do |response|
            response.iss = I18n.t('jwt.iss')
            response.sub = user&.username
            response.aud = I18n.t('jwt.aud')
            response.exp = current_time + TokenExpiration.find_by_ref('access_token').duration.minutes
            response.iat = current_time
            response.jti = access_token
            response.refresh_token = refresh_token&.encrypted_token
            response.role = user.user_role&.attributes&.slice('ref', 'name')
          end
        end
      end
    end
  end
end
