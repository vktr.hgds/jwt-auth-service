module Tokens
  module Jwt
    module Validators
      module HeaderValidator
        class << self

          def validate!(auth_header:)
            raise Errors::CommonError.new({ error: I18n.t('auth.unauthorized') }, 401) unless auth_header

            jwt_list = auth_header.split(' ')
            raise Errors::CommonError.new({ error: I18n.t('auth.missing_bearer_prefix') }, 401) if missing_bearer?(jwt_list)
            raise Errors::CommonError.new({ error: I18n.t('auth.invalid_token_format') }, 401) if invalid_format?(jwt_list)
          end

          private

          # Check if bearer string is missing from the Authorization header
          def missing_bearer?(jwt_list)
            return true unless jwt_list.first.eql? 'Bearer'

            false
          end

          # Check if the JWT has valid format
          # JWT contains 3 parts separated by dots, eg.: 123.123.123
          def invalid_format?(jwt_list)
            jwt_parts = 3
            return true unless jwt_list.last.split('.').size.eql? jwt_parts

            false
          end
        end
      end
    end
  end
end