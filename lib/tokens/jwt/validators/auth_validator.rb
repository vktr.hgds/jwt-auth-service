module Tokens
  module Jwt
    module Validators
      module AuthValidator
        class << self

          VALIDATORS = Tokens::Jwt::Validators

          private_constant :VALIDATORS

          def authorize_and_get_values(auth_header:)
            VALIDATORS::HeaderValidator.validate!(auth_header: auth_header)
            VALIDATORS::DecodeTokenValidator.validate_and_get_values!(jwt_list: auth_header.split(' '))
          end
        end
      end
    end
  end
end