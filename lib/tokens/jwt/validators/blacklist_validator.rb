module Tokens
  module Jwt
    module Validators
      module BlacklistValidator
        class << self

          def jwt_blacklisted?(request_jwt_token)
            blacklist_jwt = BlacklistJwt.find_by_encrypted_token(request_jwt_token)
            raise Errors::CommonError.new({ error: I18n.t('token.blacklist') }, 401) if blacklist_jwt
          end
        end
      end
    end
  end
end
