module Tokens
  module Jwt
    module Validators
      module DecodeTokenValidator
        class << self

          def validate_and_get_values!(jwt_list:)
            request_jwt_token = jwt_list&.last
            decoded = Tokens::JsonWebToken.decode(request_jwt_token)
            current_user = User.find(decoded[:user_id])

            [request_jwt_token, decoded, current_user]
          rescue ActiveRecord::RecordNotFound => e
            raise Errors::CommonError.new( { errors: e.message }, 404)
          rescue JWT::DecodeError, JWT::ExpiredSignature => e
            raise Errors::CommonError.new( { errors: e.message }, 401)
          end
        end
      end
    end
  end
end