module Tokens
  class JsonWebToken

    SECRET_KEY = Rails.application.secrets.secret_key_base. to_s

    private_constant :SECRET_KEY

    class << self
      def encode(payload, exp = TokenExpiration.find_by_ref(:access_token).duration.minutes.from_now)
        payload[:exp] = exp.to_i
        JWT.encode(payload, SECRET_KEY)
      end

      def decode(token)
        decoded = JWT.decode(token, SECRET_KEY)&.first
        HashWithIndifferentAccess.new decoded
      end
    end
  end
end