module Tokens
  module Jwt
    module Responses
      class SuccessResponse

        # (issuer): Issuer of the JWT
        attr_accessor :iss

        # (subject): Subject of the JWT (the user)
        attr_accessor :sub

        # (audience): Recipient for which the JWT is intended
        attr_accessor :aud

        # (expiration time): Time after which the JWT expires
        attr_accessor :exp

        # (not before time): Time before which the JWT must not be accepted for processing
        attr_accessor :nbf

        # (issued at time): Time at which the JWT was issued; can be used to determine age of the JWT
        attr_accessor :iat

        # (JWT ID): Unique identifier; can be used to prevent the JWT from being replayed (allows a tokens to be used only once)
        attr_accessor :jti

        # (Refresh Token): Refresh token
        attr_accessor :refresh_token

        # (role): The role of the user, necessary for authorization
        attr_accessor :role
      end
    end
  end
end