class User < ApplicationRecord
  has_secure_password

  belongs_to :user_role
  has_many :refresh_tokens, dependent: :delete_all
  has_many :blacklist_jwts, dependent: :delete_all

  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :username, presence: true, uniqueness: true
  validates :password,
            length: { minimum: 8 },
            if: -> { new_record? || !password.nil? }
end
