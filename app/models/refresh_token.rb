class RefreshToken < ApplicationRecord
  belongs_to :user
  before_create :set_encrypted_token, :set_expiry_date

  attr_accessor :token

  private

  def self.find_by_token(token)
    encrypted_token = Digest::SHA2.new(256).hexdigest token
    RefreshToken.find_by_encrypted_token encrypted_token
  end

  def set_encrypted_token
    self.token = SecureRandom.hex
    self.encrypted_token = Digest::SHA2.new(256).hexdigest token
  end

  def set_expiry_date
    self.expiry_date = Time.zone.now + TokenExpiration.find_by_ref('refresh_token').duration.days
  end
end
