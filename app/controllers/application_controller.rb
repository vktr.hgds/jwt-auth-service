class ApplicationController < ActionController::API

  def not_found
    render_response({ error: I18n.t('auth.not_found') },  :not_found)
  end

  # authorize/validate Authorization header and JWT validity in advance
  def authorize_request
    rescue_auth_service_errors do
      @request_jwt_token, @decoded, @current_user = Tokens::Jwt::Validators::AuthValidator.authorize_and_get_values(
        auth_header:  request.headers['Authorization']
      )
    end
  end

  protected

  # create refresh_token and access_token for sign_in request and sign_up request, or when refreshing the access_token
  def create_and_render_new_tokens(user:)
    rescue_auth_service_errors do
      body, status_code = Tokens::Generator.call(user: user)
      render_response body, status_code
    end
  end

  # renders errors if rescued
  def rescue_auth_service_errors
    yield
  rescue Errors::CommonError => e
    render_response e.message, e.status_code
  end

  # renders the json message as a responses
  def render_response(body = {}, status)
    render json: body, status: status
  end
end