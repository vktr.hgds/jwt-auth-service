class UsersController < ApplicationController
  before_action :authorize_request, except: :create
  before_action :find_user, except: :create

  # POST /users
  def create
    @user = User.new(user_params)
    @user.user_role = UserRole.find_by_ref('user')

    if @user.save
      return create_and_render_new_tokens(user: @user) if params[:stay_signed_in]
      render_response({ id: @user.id }, 201)
    else
      render_response({ errors: @user.errors.full_messages }, 500)
    end
  end

  # PUT /users/{username}
  def update
    if @user.update(user_params)
      render_response({ id: @user.id }, 201)
    else
      render_response( { errors: @user.errors.full_messages }, 500)
    end
  end

  # DELETE /users/{username}
  def destroy
    id = @user.id

    if @user.destroy
      render_response({ id: id }, 200)
    else
      render_response( { errors: @user.errors.full_messages }, 500)
    end
  end

  private

  def find_user
    @user = User.find_by_username!(params[:_username])
  rescue ActiveRecord::RecordNotFound
    render_response({ errors: I18n.t('auth.user_not_found') }, 404)
  end

  def user_params
    params.permit(
      :username, :email, :password, :password_confirmation
    )
  end
end