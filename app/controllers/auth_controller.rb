class AuthController < ApplicationController
  before_action :authorize_request, only: [:logout, :validate_token]
  before_action :jwt_blacklisted?, only: [:logout, :validate_token]
  before_action :validate_and_get_refresh_token, only: [:refresh_token]

  # POST /auth/login
  def login
    @user = User.find_by_email(params[:email])
    return render_response({ error: I18n.t('auth.unauthorized') }, 401) unless @user&.authenticate(params[:password])

    create_and_render_new_tokens(user: @user)
  end

  # POST /auth/logout
  def logout
    blacklist_jwt = BlacklistJwt.new(
      encrypted_token: @request_jwt_token,
      user_id: @current_user&.id,
      expiry_date: Time.at(@decoded.dig(:exp))
    )

    return render_response({ error: I18n.t('auth.logout_failed') }, 500) unless blacklist_jwt.save

    render_response({ success: I18n.t('auth.logout_success') }, 204)
  end

  # POST /auth/refresh-token
  # When Access-Token is expired, client needs to call this endpoint first.
  def refresh_token
    @user = @refresh_token.user
    return render_response({ error: I18n.t('token.cannot_delete') }, 409) unless @refresh_token.destroy

    create_and_render_new_tokens(user: @user)
  end

  # POST /auth/validate-token
  # When Access-Token is still valid (checked by client), we call this endpoint to authorize before using any other resource
  def validate_token
    user_with_role = @current_user.user_role&.attributes&.slice('ref', 'name')

    render_response({ success: I18n.t('auth.validated'), user: user_with_role }, 200)
  end

  private

  def validate_and_get_refresh_token
    rescue_auth_service_errors do
      @refresh_token = Tokens::RefreshTokens::Validators::RefreshTokenValidator.validate_and_get_refresh_token(
        params[:grant_type]
      )
    end
  end

  def jwt_blacklisted?
    rescue_auth_service_errors do
      Tokens::Jwt::Validators::BlacklistValidator.jwt_blacklisted?(@request_jwt_token)
    end
  end

  # allowed login parameters
  def login_params
    params.permit(:email, :password)
  end
end