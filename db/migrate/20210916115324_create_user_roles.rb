class CreateUserRoles < ActiveRecord::Migration[6.1]
  def change
    create_table :user_roles do |t|
      t.string :ref
      t.string :name

      t.timestamps
    end
    add_index :user_roles, :ref, unique: true
  end
end
