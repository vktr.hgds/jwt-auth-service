class AddExpiryDateToRefreshTokens < ActiveRecord::Migration[6.1]
  def change
    add_column :refresh_tokens, :expiry_date, :timestamp
  end
end
