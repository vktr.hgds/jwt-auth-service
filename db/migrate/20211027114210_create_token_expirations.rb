class CreateTokenExpirations < ActiveRecord::Migration[6.1]
  def change
    create_table :token_expirations do |t|
      t.string :ref
      t.string :name
      t.integer :duration
      t.string :unit_of_measurement

      t.timestamps
    end
    add_index :token_expirations, :ref, unique: true
  end
end
