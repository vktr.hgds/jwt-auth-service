class CreateRefreshTokens < ActiveRecord::Migration[6.1]
  def change
    create_table :refresh_tokens do |t|
      t.string :encrypted_token
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
    add_index :refresh_tokens, :encrypted_token, unique: true
  end
end
