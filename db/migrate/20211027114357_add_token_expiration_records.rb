class AddTokenExpirationRecords < ActiveRecord::Migration[6.1]
  def up
    access_token = TokenExpiration.new(
      ref: 'access_token', name: 'Access Token', duration: 5, unit_of_measurement: 'minutes'
    )

    refresh_token = TokenExpiration.new(
      ref: 'refresh_token', name: 'Refresh Token', duration: 30, unit_of_measurement: 'days'
    )

    access_token.save
    refresh_token.save
  end

  def down
    tokens = TokenExpiration.where(ref: %w[access_token refresh_token])
    tokens.delete_all
  end
end
