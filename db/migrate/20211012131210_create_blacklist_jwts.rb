class CreateBlacklistJwts < ActiveRecord::Migration[6.1]
  def change
    create_table :blacklist_jwts do |t|
      t.string :encrypted_token
      t.timestamp :expiry_date
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
