# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_10_27_114357) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "blacklist_jwts", force: :cascade do |t|
    t.string "encrypted_token"
    t.datetime "expiry_date"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_blacklist_jwts_on_user_id"
  end

  create_table "refresh_tokens", force: :cascade do |t|
    t.string "encrypted_token"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "expiry_date"
    t.index ["encrypted_token"], name: "index_refresh_tokens_on_encrypted_token", unique: true
    t.index ["user_id"], name: "index_refresh_tokens_on_user_id"
  end

  create_table "token_expirations", force: :cascade do |t|
    t.string "ref"
    t.string "name"
    t.integer "duration"
    t.string "unit_of_measurement"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ref"], name: "index_token_expirations_on_ref", unique: true
  end

  create_table "user_roles", force: :cascade do |t|
    t.string "ref"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ref"], name: "index_user_roles_on_ref", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "email"
    t.string "password_digest"
    t.bigint "user_role_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["user_role_id"], name: "index_users_on_user_role_id"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "blacklist_jwts", "users"
  add_foreign_key "refresh_tokens", "users"
  add_foreign_key "users", "user_roles"
end
