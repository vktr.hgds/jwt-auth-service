Rails.application.routes.draw do
  resources :users, param: :_username

  post '/auth/login', to: 'auth#login'
  post '/auth/logout', to: 'auth#logout'
  post '/auth/validate_token', to: 'auth#validate-token'
  post '/auth/refresh_token', to: 'auth#refresh-token'

  get '/*a', to: 'application#not_found'

end
