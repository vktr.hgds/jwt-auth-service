every 5.minutes do
  set :output, "log/cron.log"
  rake 'blacklist_jwts:delete_expired'
end